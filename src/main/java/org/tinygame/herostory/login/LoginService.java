package org.tinygame.herostory.login;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tinygame.herostory.MySqlSessionFactory;
import org.tinygame.herostory.async.AsyncOperationProcessor;
import org.tinygame.herostory.async.IAsyncOperation;
import org.tinygame.herostory.login.db.IUserDao;
import org.tinygame.herostory.login.db.UserEntity;

import java.util.function.Function;

/**
 * 登陆服务
 */
public final class LoginService {
    /**
     * 日志服务
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginService.class);

    /**
     * 单例对象
     */
    private static final LoginService _instance = new LoginService();

    /**
     * 私有化类默认构造器
     */
    private LoginService() {
    }

    /**
     * 获取单例对象
     *
     * @return
     */
    public static LoginService getInstance() {
        return _instance;
    }

    /**
     * 用户登陆
     *
     * @param userName 用户名称
     * @param password 密码
     * @param callback 回调函数
     * @return
     */
    public void userLogin(String userName, String password, Function<UserEntity,Void> callback) {
        if (null == userName ||
            null == password) {
            return;
        }
        IAsyncOperation asyncOp = new AsyncGetUserByName(userName,password) {

            @Override
            public void doFinish() {
                if (null != callback) {
                    callback.apply(this.get_userEntity());
                }
            }
        };

        AsyncOperationProcessor.getInstance().process(asyncOp);

    }

    /**
     * 异步方式获取用户
     */
    private class AsyncGetUserByName implements IAsyncOperation {
        /**
         * 用户名称
         */
        private final String _userName;
        /**
         * 密码
         */
        private final String _password;

        /**
         * 用户对象
         */
        private UserEntity _userEntity = null;

        public AsyncGetUserByName(String _userName, String _password) {
            this._userName = _userName;
            this._password = _password;
        }

        /**
         * 获取用户实体
         * @return 用户实体
         */
        public UserEntity get_userEntity() {
            return _userEntity;
        }

        @Override
        public int bindId() {
            return _userName.charAt(_userName.length() - 1);
        }

        @Override
        public void doAsync() {
            try (SqlSession mySqlSession = MySqlSessionFactory.openSession()) {
                // 获取 DAO
                IUserDao dao = mySqlSession.getMapper(IUserDao.class);
                // 获取用户实体
                UserEntity userEntity = dao.getByUserName(_userName);

                LOGGER.info("当前线程 = {}", Thread.currentThread().getName());

                if (null != userEntity) {
                    if (!_password.equals(userEntity.password)) {
                        LOGGER.error("用户密码错误，userId = {}, userName = {}",userEntity.userId,_userName);
                    }
                } else {
                    userEntity = new UserEntity();
                    userEntity.userName = _userName;
                    userEntity.password = _password;
                    userEntity.heroAvatar = "Hero_Shaman";

                    dao.insertInto(userEntity);
                }
                _userEntity = userEntity;
            } catch (Exception ex) {
                // 记录错误日志
                LOGGER.error(ex.getMessage(), ex);
            }
        }
    }
}
