package org.tinygame.herostory.model;

/**
 * 用户
 */
public class User {
    /**
     * 用户ID
     */
    public int userId;

    /**
     * 用户名称
     */
    public String userName;

    /**
     * 英雄形象
     */
    public String hereAvatar;

    /**
     * 当前血量
     */
    public int currHp;

    /**
     * 移动状态
     */
    public final MoveState moveState = new MoveState();
}
