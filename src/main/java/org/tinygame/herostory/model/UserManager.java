package org.tinygame.herostory.model;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class UserManager {
    /**
     * 私有化构造器
     */
    private UserManager(){

    }

    /**
     * 服务器端维护用户信息的容器，用户上线加入到这个map中，用户下线从map中移除
     */
    private static final Map<Integer, User> _userMap = new ConcurrentHashMap<>();

    /**
     * 添加用户
     * @param newUser
     */
    public static void addUser(User newUser){
        if (null!= newUser){
            _userMap.put(newUser.userId,newUser);
        }
    }

    /**
     * 根据用户ID移除用户
     * @param userId 用户Id
     */
    public static void removeUserById(int userId){
        _userMap.remove(userId);
    }

    /**
     * 获取所有的用户列表
     * @return 所有的用户列表
     */
    public static Collection<User> listUser(){
        return _userMap.values();
    }

    /**
     * 根据Id获取用户
     * @param userId 用户Id
     * @return 用户对象
     */
    public static User getUserById(Integer userId) {
        return _userMap.get(userId);
    }
}
